/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Erick
 */
public class Cliente implements Serializable{
    
    private String email;
    private String nomeCliente;
    private String sexo;
    private int pontos;
    private ArrayList<Ingresso> listaIngressos = new ArrayList<>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public ArrayList<Ingresso> getListaIngressos() {
        return listaIngressos;
    }

    public void setListaIngressos(ArrayList<Ingresso> listaIngressos) {
        this.listaIngressos = listaIngressos;
    }

    public void addIngresso(Ingresso i) {
        this.listaIngressos.add(i);
    }
    
    public void pontos(int p) {
        this.pontos += p;
    }
    
    public Cliente(String email, String nome, String sexo) {
        this.email = email;
        this.nomeCliente = nome;
        this.sexo = sexo;
        pontos = 0;
    }
    
}
