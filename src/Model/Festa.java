/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author Erick
 */
public class Festa implements Serializable{
    private int idFesta;
    private String nomeFesta;
    private String dataFesta;
    private String local;
    private String tipoFesta;
    private int nIngressos;
    private int pontuacao;
    private double valor;

    public int getIdFesta() {
        return idFesta;
    }

    public void setIdFesta(int idFesta) {
        this.idFesta = idFesta;
    }

    public String getNomeFesta() {
        return nomeFesta;
    }

    public void setNomeFesta(String nomeFesta) {
        this.nomeFesta = nomeFesta;
    }

    public String getDataFesta() {
        return dataFesta;
    }

    public void setDataFesta(String dataFesta) {
        this.dataFesta = dataFesta;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getTipoFesta() {
        return tipoFesta;
    }

    public void setTipoFesta(String tipoFesta) {
        this.tipoFesta = tipoFesta;
    }

    public int getnIngressos() {
        return nIngressos;
    }

    public void setnIngressos(int nIngressos) {
        this.nIngressos = nIngressos;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
    
    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public Ingresso comprar(Cliente c) {
           
        Ingresso i = new Ingresso(c, this);
        i.codigo++;    
        c.pontos(this.pontuacao);
        return i;     
        
    }
    
    public Festa (int idFesta, String nomeFesta, String dataFesta, String local, String tipoFesta, int pontuacao, double valor){
        this.idFesta = idFesta;
        this.dataFesta = dataFesta;
        this.local = local;
        this.nomeFesta = nomeFesta;
        this.pontuacao = pontuacao;
        this.tipoFesta = tipoFesta;
        this.valor = valor;
    }
    
    
}
