/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author Erick
 */
public class Ingresso implements Serializable{
    
    private int idFesta;
    private String email;
    private Festa festa;
    private Cliente cliente;
    public int codigo=1000;
    
    
    

    /*public int getIdFesta() {
        return idFesta;
    }

    public void setIdFesta(int idFesta) {
        this.idFesta = idFesta;
    }*/

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public Ingresso(Cliente cl, Festa f) {
        this.cliente = cl;
        this.festa = f;
        
        this.codigo = codigo++;
        
        cl.addIngresso(this);
    }
    
    
    public Festa getFesta() {
        return festa;
    }
    
    
    
}
