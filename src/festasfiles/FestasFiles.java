/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package festasfiles;

import FXML.TelaInicialController;
import Model.Arquivos;
import Model.Cliente;
import Model.Festa;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Erick
 */
public class FestasFiles extends Application {
    
    public static List<Festa> festas = Arquivos.loadFesta();
    public static List<Cliente> clientes = Arquivos.loadCliente();
    
    public static Cliente usuario = null;
    
    public static Stage stage;
    
    public static void main (String[] args){
        launch(args);
        
        Arquivos.saveFestas(festas);
        Arquivos.saveCliente(clientes);
    }
    
    private Stage primaryStage; //isso não havia em outras classes
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.setTitle("Administrador de Festas");
        primaryStage.setScene(getRootLayout());
        primaryStage.setFullScreen(false);
        primaryStage.show();
        
        
    }
    
     private Scene getRootLayout() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaInicialController.class.getResource("TelaInicial.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
            
            //observe essas duas linhas
            TelaInicialController tc = loader.getController();
            tc.setStage(primaryStage);
            
            scene = new Scene(rootLayout);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }

    
    
    
}
