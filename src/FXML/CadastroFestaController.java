/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import Model.Cliente;
import Model.Festa;
import Tela.Controller;
import festasfiles.FestasFiles;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Erick
 */
public class CadastroFestaController extends Controller {

    @FXML
    private TextField idFesta;
    @FXML
    private TextField nome;
    @FXML
    private TextField data;
    @FXML
    private TextField local;
    @FXML
    private TextField tipo;
    @FXML
    private TextField pontos;
    @FXML
    private TextField valor;
    @FXML
    private Button voltar;
    @FXML
    private Button cadastrar;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }


    @FXML
    public void chamaTelaInicial(){
        this.stage.setScene(telaInicial());
        this.stage.setFullScreen(false);
    }


    private Scene telaInicial() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaInicialController.class.getResource("TelaInicial.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              
            TelaInicialController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    public void cadastrarFesta(){
        Festa f = new Festa(Integer.parseInt(idFesta.getText()),nome.getText(), data.getText(), local.getText(), tipo.getText(),Integer.parseInt(pontos.getText()),Double.parseDouble(valor.getText()));

        FestasFiles.festas.add(f);
        
        chamaTelaInicial();
    }
    
}
