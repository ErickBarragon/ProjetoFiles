/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import Model.Cliente;
import Model.Festa;
import Tela.Controller;
import festasfiles.FestasFiles;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Erick
 */
public class ComprarIngressoController extends Controller {

    @FXML
    private ListView<Festa> listaFestas;
    private ObservableList <Festa> obsFestas;
    @FXML
    private TextField email;
    @FXML
    private Button voltar;
    @FXML
    private Button comprar;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        obsFestas = listaFestas.getItems();
        
        obsFestas.addAll(FestasFiles.festas);
    }    
    @FXML
    public void chamaTelaInicial(){
        this.stage.setScene(telaInicial());
        this.stage.setFullScreen(false);
    }

    
    private void cellFactory() {
        listaFestas.setCellFactory(param -> new ListCell<Festa>() {
            @Override
            public void updateItem(Festa festa, boolean empty) {
                super.updateItem(festa, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(festa.getNomeFesta() + ": " + festa.getTipoFesta() + " - " + festa.getDataFesta() + " em " + festa.getLocal() + " por: R$" + festa.getValor());
                }
                setGraphic(null);
            }
        });
    }

    private Scene telaInicial() {

        Scene scene = null;
        try {
            
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaInicialController.class.getResource("TelaInicial.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              
            TelaInicialController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
   
    @FXML
    public void comprarIngresso() {
         Festa f = listaFestas.getSelectionModel().getSelectedItem();
         
         Cliente c = null;
         for(Cliente cl : FestasFiles.clientes) {
             if (cl.getEmail().equals(email.getText())) {
                 c = cl;
             }
         }
         
         f.comprar(c);
         
         chamaTelaInicial();
     }
}
