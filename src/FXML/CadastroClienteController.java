/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import Model.Cliente;
import festasfiles.FestasFiles;
import Tela.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Erick
 */
public class CadastroClienteController extends Controller {

    @FXML
    private TextField email;
    @FXML
    private TextField nome;
    @FXML
    private RadioButton m;
    @FXML
    private RadioButton f;
    @FXML
    private Button voltar;
    @FXML
    private Button cadastrar;
    
    private String sexo;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void chamaTelaInicial(){
        this.stage.setScene(telaInicial());
        this.stage.setFullScreen(false);
    }


    private Scene telaInicial() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TelaInicialController.class.getResource("TelaInicial.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              
            TelaInicialController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    @FXML
    public void ideologiaDeGenero(){
        if(m.isSelected()){
            sexo = "Masculino";
        }
        if(f.isSelected()){
            sexo = "Feminino";
        }
        cadastrarCliente();
    }
    
    
    public void cadastrarCliente(){
        
        Cliente c = new Cliente(email.getText(),nome.getText() ,sexo );

        FestasFiles.clientes.add(c);
        chamaTelaInicial();
    }
}
