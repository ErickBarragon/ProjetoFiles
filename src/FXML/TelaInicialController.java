/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML;

import Tela.Controller;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Erick
 */
public class TelaInicialController extends Controller {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private Button cadastroCliente;
    @FXML
    private Button cadastroFesta;
    @FXML
    private Button comprarIngresso;
    @FXML
    private Button verPontos;
    
    
    
    @FXML
    public void chamaCadastroCliente(){
        this.stage.setScene(cadastroCliente());
        this.stage.setFullScreen(false);
    }
    @FXML
    public void chamaCadastroFesta(){
        this.stage.setScene(cadastroFesta());
        this.stage.setFullScreen(false);
    }
    @FXML
    public void chamaComprarIngresso(){
        this.stage.setScene(comprarIngresso());
        this.stage.setFullScreen(false);
    }    
    @FXML
    public void chamaVerPontos(){
        this.stage.setScene(verPontos());
        this.stage.setFullScreen(false);
    }
    

    
    private Scene cadastroCliente() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CadastroClienteController.class.getResource("CadastroCliente.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            CadastroClienteController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene cadastroFesta() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CadastroFestaController.class.getResource("CadastroFesta.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            CadastroFestaController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene comprarIngresso() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ComprarIngressoController.class.getResource("ComprarIngresso.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            ComprarIngressoController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    private Scene verPontos() {

        Scene scene = null;
        try {
            // Loader
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(VerPontosController.class.getResource("VerPontos.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();

            scene = new Scene(rootLayout);
              //observe essas duas linhas
            VerPontosController tc = loader.getController();
            tc.setStage(this.stage);

        } catch (IOException e) {
            System.out.println("Erro!");
            e.printStackTrace();
        }
        return scene;
    }
    
    
    
    
}
